from django import forms
from .models import ESL
from base_station.models import  Store

class eslForm(forms.ModelForm):

    class Meta:
        model = ESL
        fields = ['commodity_barcode','esl_barcode','base_station_code']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
    

class ViewStoreForm(forms.ModelForm):

    class Meta:
        model = Store
        fields = '__all__'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            self.fields[field].disabled = True        
    
        
class StoreForm(forms.ModelForm):

    class Meta:
        model = Store
        fields = '__all__'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
    