
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('add', views.esl_add, name='addesl'),    
    path('getdata', views.esl_get, name='getesl'),    
    path('updatedata/<int:id>', views.esl_update, name='updateesl'),    
    path('delete/<int:id>', views.esl_delete, name='deleteesl'),    

    path('store_add', views.store_add, name='addstore'),    
    path('store_getdata', views.store_get, name='getstore'),    
    path('store_updatedata/<int:id>', views.store_update, name='updatestore'),    
    path('store_delete/<int:id>', views.store_delete, name='deletestore'),    
]
 