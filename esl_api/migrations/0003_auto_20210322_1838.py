# Generated by Django 3.1.5 on 2021-03-22 13:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('esl_api', '0002_auto_20210228_1336'),
    ]

    operations = [
        migrations.AlterField(
            model_name='esl',
            name='esl_size',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='ESL Size'),
        ),
        migrations.AlterField(
            model_name='esl',
            name='powermeter',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='Power Meter'),
        ),
    ]
