from django.shortcuts import render
from esl_api.models import ESL
from base_station.models import Store
# Create your views here.

import json
import time
import requests
from django.core import serializers
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView

from django.contrib import messages
from django.conf import settings
from django.db import IntegrityError
from django.http import HttpResponse, FileResponse, Http404, JsonResponse
import os
from django.db.models import Q


from rest_framework.views import APIView
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.decorators import api_view

from django_filters.rest_framework import DjangoFilterBackend
from django.views.decorators.csrf import csrf_exempt
from .serializers import eslSerializer

@csrf_exempt
@api_view(['GET'])
def esl_get(request):
    queryset = ESL.objects.all()    
    serializer = eslSerializer(queryset, many=True)
    return JsonResponse({'esl': serializer.data}, safe=False, status=status.HTTP_200_OK)    

@api_view(['GET'])
def esl_delete(request, id):
    esl = get_object_or_404(ESL, id=id)
    ESL.objects.filter(id=id).delete()
    context = {
        'status': 'success',
        'Message': f"esl {esl.id} has been deleted"
    }
    return Response(context)

 
@csrf_exempt
@api_view(['POST'])
def esl_update(request,id):
    if request.method == 'POST':
        print(request.POST.get('Status'))
        esl = get_object_or_404(ESL, id=id)                        
        esl.base_station_code_id  = request.POST.get('base_station_code') if request.POST.get('base_station_code') != "" else esl.base_station_code
        esl.commodity_barcode_id  = request.POST.get('commodity_barcode') if request.POST.get('commodity_barcode') != "" else esl.commodity_barcode
        esl.esl_barcode  = request.POST.get('esl_barcode') if request.POST.get('esl_barcode') != "" else esl.esl_barcode
        esl.esl_size  = request.POST.get('esl_size') if request.POST.get('esl_size') != "-1" else esl.esl_size
        esl.esl_number  = request.POST.get('esl_number') if request.POST.get('esl_number') != "" else esl.esl_number
        esl.esl_version  = request.POST.get('esl_version') if request.POST.get('esl_version') != "" else esl.esl_version
        esl.powermeter  = request.POST.get('powermeter') if request.POST.get('powermeter') != "-1" else esl.powermeter
        esl.save()
        context = {
            'id': esl.id,                
        }
        return Response(context)

@csrf_exempt
@api_view(['POST'])
def esl_add(request):
    if request.method == 'POST':
        
        esl = ESL()            
        esl.base_station_code_id = request.POST.get('base_station_code')
        esl.commodity_barcode_id  = request.POST.get('commodity_barcode')
        esl.esl_barcode  = request.POST.get('esl_barcode')                    
        esl.esl_size =    request.POST.get('esl_size')
        esl.esl_number =    request.POST.get('esl_number')
        esl.esl_version =    request.POST.get('esl_version')
        esl.powermeter =    request.POST.get('powermeter')    
        esl.save()
        context = {
            'id': esl.id,
            'commodity_barcode': esl.commodity_barcode,
            'esl_barcode': esl.esl_barcode,        
            'esl_size' : esl.esl_size,
            'esl_number' : esl.esl_number,
            'esl_version' : esl.esl_version,
            'powermeter' : esl.powermeter
        }
        return Response(context)        
    
    else:
        
        return Response({"status": "fail",
                         "message": "method is not allowed"})
    
@csrf_exempt
@api_view(['GET'])
def store_get(request):
    queryset = Store.objects.all()    
    serializer = storeSerializer(queryset, many=True)
    return JsonResponse({'store': serializer.data}, safe=False, status=status.HTTP_200_OK)    

@api_view(['GET'])
def store_delete(request, id):
    store = get_object_or_404(Store, id=id)
    Store.objects.filter(id=id).delete()
    context = {
        'status': 'success',
        'Message': f"store {store.id} has been deleted"
    }
    return Response(context)

@csrf_exempt
@api_view(['POST'])
def store_update(request,id):
    if request.method == 'POST':
        print(request.POST.get('Status'))
        store = get_object_or_404(Store, id=id)                        
        store.facility_address = request.POST.get('facility_address') if request.POST.get('facility_address') != "" else store.facility_address
        store.facility_name = request.POST.get('facility_name') if request.POST.get('facility_name') != "" else store.facility_name        
        store.save()
        context = {
            'id': store.id,                
        }
        return Response(context)

@csrf_exempt
@api_view(['POST'])
def store_add(request):
    if request.method == 'POST':
        
        store = Store()
        store.facility_address = request.POST.get('facility_address')
        store.facility_name = request.POST.get('facility_name')  
        store.save()
        context = {
            'id': store.id,
            'facility_address': store.facility_address,
            'facility_name': store.facility_name            
        }
        return Response(context)
    else:
        
        return Response({"status": "fail",
                         "message": "method is not allowed"})