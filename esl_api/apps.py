from django.apps import AppConfig


class EslApiConfig(AppConfig):
    name = 'esl_api'
