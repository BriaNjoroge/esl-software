from rest_framework import serializers
from . import models
from base_station.models import Store

class eslSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ESL
        fields = "__all__"

class storeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = "__all__"