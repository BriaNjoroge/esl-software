from django.db import models
from commodity_api.models import Commodities
from base_station.models import BaseStation

# Create your models here.


class ESL(models.Model):
    
    base_station_code = models.ForeignKey(BaseStation,related_name="base_station",on_delete=models.CASCADE)
    commodity_barcode = models.ForeignKey(Commodities,related_name="commodity",on_delete=models.CASCADE)
    esl_barcode = models.CharField('ESL Barcode', max_length=255, null=True, blank=True)    
    esl_size = models.FloatField('ESL Size',default=0, null=True, blank=True) ##(can take for now 1.54, 2.19, 2.79)
    esl_number = models.CharField('ESL Number', max_length=255, null=True, blank=True)
    esl_version = models.CharField('ESL Version', max_length=255, null=True, blank=True)
    powermeter = models.FloatField('Power Meter',default=0, null=True, blank=True) ##(can take for now 1.54, 2.19, 2.79)
    