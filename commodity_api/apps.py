from django.apps import AppConfig


class CommodityApiConfig(AppConfig):
    name = 'commodity_api'
