from django.db import models

# Create your models here.

class Commodities(models.Model):
    commodity_code = models.CharField('Commodity Code', max_length=255, null=True, blank=True)
    commodity_barcode = models.CharField('Commodity BarCode', max_length=255, null=True, blank=True)
    own_code = models.CharField('Own Code', max_length=255, null=True, blank=True)
    unit = models.IntegerField('code',default=1)
    commodity_name = models.CharField('Commodity Name', max_length=255, null=True, blank=True)
    commodity_abbr = models.CharField('Commodity Abbr', max_length=255, null=True, blank=True)
    template_category = models.CharField('Template Category ', max_length=255, null=True, blank=True)
    template_type = models.CharField('Template Type ', max_length=255, null=True, blank=True)
    origin = models.CharField('Origin', max_length=255, null=True, blank=True)
    lavel = models.CharField('Lavel', max_length=255, null=True, blank=True)
    qrcode_link = models.CharField('QRCode Link', max_length=255, null=True, blank=True)
    picture_info = models.ImageField("Picture Info",upload_to='commodity')
    specification = models.CharField('Specification', max_length=255, null=True, blank=True)
    selling_price = models.FloatField('Selling Price',default=0)
    original_price = models.FloatField('Original Price',default=0)
    vip_price = models.FloatField('VIP Price',default=0)
    stock_1 = models.FloatField('Stock 1',default=0)
    stock_2 = models.FloatField('Stock 2',default=0)
    stock_3  = models.FloatField('Stock 3',default=0)
    promotion_period_start_date = models.DateTimeField('Promotion Period Start Date', null=True, blank=True)
    promotion_period_end_date = models.DateTimeField('Promotion Period end Date', null=True, blank=True)

    def __str__(self):
        return self.commodity_barcode
