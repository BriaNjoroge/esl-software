 
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('add', views.commodity_add, name='addcommodity'),    
    path('getdata', views.commodity_get, name='getcommodity'),    
    path('updatedata/<int:id>', views.commodity_update, name='updatecommodity'),    
    path('delete/<int:id>', views.commodity_delete, name='deletecommodity'),    
]
 