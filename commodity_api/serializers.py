from rest_framework import serializers
from . import models

class CommoditySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Commodities
        fields = "__all__"