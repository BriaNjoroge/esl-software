from django import forms
from django.forms import ClearableFileInput
from commodity_api.models import Commodities
from django.utils.safestring import mark_safe


class ImagePreviewWidget(forms.widgets.FileInput):
    def render(self, name, value, attrs=None, **kwargs):
        input_html = super().render(name, value, attrs=None, **kwargs)
        img_html = mark_safe(f'<br><br><img class="col-md-12" src="{value.url}"/>')
        return f'{input_html}{img_html}'


class DateInput(forms.DateInput):
    input_type = 'date'

class ViewCommoditiesForm(forms.ModelForm):
    class Meta:
        model = Commodities
        fields = '__all__'
        widgets = {
            'promotion_period_start_date': DateInput(),
            'promotion_period_end_date': DateInput(),
            
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['picture_info'] = forms.ImageField(widget=ImagePreviewWidget)
        
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            self.fields[field].disabled = True
        

class UpdateCommoditiesForm(forms.ModelForm):
    class Meta:
        model = Commodities
        fields = '__all__'
        widgets = {
            'promotion_period_start_date': DateInput(),
            'promotion_period_end_date': DateInput(),
            
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
