from django.shortcuts import render

from base_station.models import BaseStation
from commodity_api.models import Commodities
# from base_station.forms import AddBaseStationForm
# Create your views here.

import json
import time
import requests
from django.core import serializers
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView

from django.contrib import messages
from django.conf import settings
from django.db import IntegrityError
from django.http import HttpResponse, FileResponse, Http404, JsonResponse
import os
from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.decorators import api_view

from django_filters.rest_framework import DjangoFilterBackend
from django.views.decorators.csrf import csrf_exempt
from .serializers import CommoditySerializer

@csrf_exempt
@api_view(['GET'])
def commodity_get(request):
    queryset = Commodities.objects.all()    
    serializer = CommoditySerializer(queryset, many=True)
    return JsonResponse({'commodity': serializer.data}, safe=False, status=status.HTTP_200_OK)    

@api_view(['GET'])
def commodity_delete(request, id):
    commodity = get_object_or_404(Commodities, id=id)
    Commodities.objects.filter(id=id).delete()
    context = {
        'status': 'success',
        'Message': f"Job description {commodity.id} has been deleted"
    }
    return Response(context)

 
@csrf_exempt
@api_view(['POST'])
def commodity_update(request,id):
    if request.method == 'POST':
        print("request.FILES",request.FILES)
        print(request.POST)
        
        commodity = get_object_or_404(Commodities, id=id)                
        commodity.commodity_code = request.POST.get('commodity_code') if request.POST.get('commodity_code') != "" else commodity.commodity_code
        commodity.commodity_barcode = request.POST.get('commodity_barcode') if request.POST.get('commodity_barcode') != "" else commodity.commodity_barcode
        commodity.own_code = request.POST.get('own_code') if request.POST.get('own_code') != "" else commodity.own_code
        commodity.unit = request.POST.get('unit') if request.POST.get('unit') != "" else commodity.unit
        commodity.commodity_name = request.POST.get('commodity_name') if request.POST.get('commodity_name') != "" else commodity.commodity_name
        commodity.commodity_abbr = request.POST.get('commodity_abbr') if request.POST.get('commodity_abbr') != "" else commodity.commodity_abbr
        commodity.template_category = request.POST.get('template_category') if request.POST.get('template_category') != "" else commodity.template_category
        commodity.template_type = request.POST.get('template_type') if request.POST.get('template_type') != "" else commodity.template_type
        commodity.origin = request.POST.get('origin') if request.POST.get('origin') != "" else commodity.origin
        commodity.lavel = request.POST.get('lavel') if request.POST.get('lavel') != "" else commodity.lavel
        commodity.qrcode_link = request.POST.get('qrcode_link') if request.POST.get('qrcode_link') != "" else commodity.qrcode_link
        commodity.picture_info = request.FILES.get('picture_info') if request.FILES.get('picture_info') != "" else commodity.picture_info
        commodity.specification = request.POST.get('specification') if request.POST.get('specification') != "" else commodity.specification
        commodity.selling_price = request.POST.get('selling_price') if request.POST.get('selling_price') != "-1" else commodity.selling_price
        commodity.original_price = request.POST.get('original_price') if request.POST.get('original_price') != "-1" else commodity.original_price
        commodity.vip_price = request.POST.get('vip_price') if request.POST.get('vip_price') != "-1" else commodity.vip_price
        commodity.stock_1 = request.POST.get('stock_1') if request.POST.get('stock_1') != "-1" else commodity.stock_1
        commodity.stock_2 = request.POST.get('stock_2') if request.POST.get('stock_2') != "-1" else commodity.stock_2
        commodity.stock_3  = request.POST.get('stock_3') if request.POST.get('stock_3') != "-1" else commodity.stock_3
        commodity.promotion_period_start_date = request.POST.get('promotion_period_start_date') if request.POST.get('promotion_period_start_date') != "" else commodity.promotion_period_start_date
        commodity.promotion_period_end_date = request.POST.get('promotion_period_end_date') if request.POST.get('promotion_period_end_date') != "" else commodity.promotion_period_end_date
        commodity.save()
        context = {
            'id': commodity.id,                
        }
        return Response(context)

@csrf_exempt
@api_view(['POST'])
def commodity_add(request):
    if request.method == 'POST':
        print("request.FILES",request.FILES)
        print(request.POST)
        
        # form = AddBaseStationForm(request.POST)
        # if form.is_valid():
        commodity = Commodities()
        commodity.commodity_code = request.POST.get('commodity_code')
        commodity.commodity_barcode = request.POST.get('commodity_barcode')
        commodity.own_code = request.POST.get('own_code')
        commodity.unit = request.POST.get('unit')
        commodity.commodity_name = request.POST.get('commodity_name')
        commodity.commodity_abbr = request.POST.get('commodity_abbr')
        commodity.template_category = request.POST.get('template_category')
        commodity.template_type = request.POST.get('template_type')
        commodity.origin = request.POST.get('origin')
        commodity.lavel = request.POST.get('lavel')
        commodity.qrcode_link = request.POST.get('qrcode_link')
        commodity.picture_info = request.FILES.get('picture_info')
        commodity.specification = request.POST.get('specification')
        commodity.selling_price = request.POST.get('selling_price')
        commodity.original_price = request.POST.get('original_price')
        commodity.vip_price = request.POST.get('vip_price')
        commodity.stock_1 = request.POST.get('stock_1')
        commodity.stock_2 = request.POST.get('stock_2')
        commodity.stock_3  = request.POST.get('stock_3')
        commodity.promotion_period_start_date = request.POST.get('promotion_period_start_date')
        commodity.promotion_period_end_date = request.POST.get('promotion_period_end_date')          
        commodity.save()
        context = {
            'id': commodity.id,                
        }
        return Response(context)
        # else:        
        #     return Response({"status": "fail",
        #                     "message": "form is invalid"})
    
    else:
        
        return Response({"status": "fail",
                         "message": "method is not allowed"})
