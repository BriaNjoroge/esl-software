# ESL

```
This is an application to manage ESL.
```


---
## Prerequisite softwares

Make sure your computer has the following softwares.

1. Python :snake:
2. Mysql

---
## Set up the environment
Download the project or clone it
```bash
git clone URL
```
#### Activate the environment

Open your terminal/cmd in the project folder and run

```bash
virtualenv venv
```

For windows system :computer:
```bash
.\venv\scripts\activate
```

For Linux systems :penguin:
```bash
source .\venv\bin\activate
```
---

## Run the project
In the terminal, Run the following commands:


1. Install the required python libraries
    ```bash
    python -m pip install --upgrade pip
    ```

    ```bash
    pip install -r requirements.txt
    ```

    pip install mysqlclient

2. Configure Mysql database
    ```bash
    i.  Create database in mysql eg. ESL
    ii. Change your database name, username and password in settings.py file. 
        eg.
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'NAME': 'ESL',
                'USER': 'root',
                'PASSWORD': '',
                'HOST': '127.0.0.1',
                'PORT': '3306',
            }
        }
    ```

3. Migration of the database
    ```bash
    python manage.py makemigrations
    ```
    ```bash
    python manage.py migrate
    ```
4. Run the project
    ```bash
    python manage.py runserver
    ```
---


## Usage
Open Browser and Run project url eg.http://127.0.0.1:8000/ start the journey :smile:

1. Register yourself to use software

    **Fill out the form and click submit** <br>
    **If there is any validation you can see on you template** <br>
    **If signup is sucseccfull you will redirect to signin** <br>
    
2. Login

    **Fill out the login crediential and click submit** <br>
    **If there is any validation you can see on you template** <br>
    **If signin is sucseccfull you will redirect to Dashboard** <br>


3. Dashboard

    **Left side is sidebar where you can traverse through the pages** <br>
    **Top right we have logout button** <br>

4. Esl, Base station, Commodities, Store
    **All pages have Add, update, delete option on the same page** <br>


:sunny: :sunny:THANK YOU :sunny::sunny:
