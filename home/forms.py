from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.core import validators
import re

from django import forms
from django.contrib.auth.password_validation import validate_password

from home.models import Esl_User


class UserRegForm(forms.Form):

    username = forms.CharField(label='EMAIL *',
                               widget=forms.TextInput(attrs={'placeholder': 'EMAIL', 'class': "form-control"}))

    password = forms.CharField(label='Password *', validators=[validate_password],
                               widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': "form-control"}))

    confirm_password = forms.CharField(label='Confirm Password *', widget=forms.PasswordInput(
        attrs={'placeholder': 'Confirm Password', 'class': "form-control"}))
    
    firstName = forms.CharField(label='First Name *',
                               widget=forms.TextInput(attrs={'placeholder': 'First Name', 'class': "form-control"}))
    lastName = forms.CharField(label='Last Name *',
                               widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control"}))
    mobileNo = forms.IntegerField(label='Mobile Number *',
                                widget=forms.NumberInput(attrs={"min":0,'placeholder': ' mobile number', 'class': "form-control"}))




class ChangePwdForm(forms.Form):
    new_password = forms.CharField( widget=forms.PasswordInput(attrs={'class':'form-control'}), label='New Password', max_length=100, help_text='Password should contains one special character, one uppercase and one lowercase value and minimum length is 8.')
    confirm_new_password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}), label='Confirm New Password', max_length=100)

    def clean(self):
        new_password = self.cleaned_data['new_password']
        confirm_new_password = self.cleaned_data['confirm_new_password']
        if new_password != confirm_new_password:
            raise forms.ValidationError({'new_password': ["password and confirm password does not match",]})
        if len(new_password) < 8:
            raise forms.ValidationError({'new_password': ["Password minimum length was 8",]})

        letters = set(new_password)
        mixed = any(letter.isdigit() for letter in letters) and any(letter.islower() for letter in letters) and any(letter.isupper() for letter in letters)
        if not mixed:
            raise forms.ValidationError({'new_password': ["Invalid password: Mixed case characters not detected",]})


        if not re.findall('[^A-Za-z0-9]', new_password):
            raise forms.ValidationError({'new_password': ["Invalid password: special characters not detected",]})


class UserForm(forms.ModelForm):

    class Meta:
        model = Esl_User
        fields = ('firstName', 'lastName','mobileNo')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

    def clean(self):
        cleaned_data = super(UserForm, self).clean()
        firstName = cleaned_data.get("firstName")
        lastName = cleaned_data.get("lastName")
        mobileNo = cleaned_data.get("mobileNo")


