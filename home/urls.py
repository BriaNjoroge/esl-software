
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.home, name='index'),
    path('signup', views.sign_up, name='signup'),

    path('activate/<slug:uidb64>/<slug:token>/',
         views.activate_account, name='activate'),
    path('signin', views.sign_in, name='signin'),
    path('password/update', views.profile_update, name='profile_update'),
    path('profile/<int:id>/update', views.user_details_update, name='user_details_update'),
    path('logout', views.logoutfun, name='logout'),
    
    path('basestation', views.basestation, name='basestation'),
path('basestationviewform/<int:id>', views.basestationviewform, name='basestationviewform'),
    
    path('basestationupdateform/<int:id>', views.basestationupdateform, name='basestationupdateform'),
    path('basestationupdate/<int:id>', views.basestationupdate, name='basestationupdate'),
    path('basestationdelete/<int:id>', views.basestationdelete, name='basestationdelete'),

    path('productmanager/productquery', views.commodities, name='commodities'),
    path('productmanager/productquery/commoditiesdelete/<int:id>', views.commoditiesdelete, name='commoditiesdelete'),
    path('productmanager/productquery/commoditiesupdate/<int:id>', views.commoditiesupdate, name='commoditiesupdate'),
    path('productmanager/productquery/commoditieupdateform/<int:id>', views.commoditieupdateform, name='commoditieupdateform'),
    path('productmanager/productquery/commoditieviewform/<int:id>', views.commodityviewform, name='commodityviewform'),

    path('templatemanager/storetempate', views.storetempate, name='storetempate'),
    path('templatemanager/createtempate', views.create_template, name='create_template'),
    path('templatemanager/edittemplate', views.edittemplate, name='edittemplate'),
   
    path('equipment/pricetag', views.esl, name='esl'),
    path('equipment/pricetag/update/<int:id>', views.eslupdate, name='eslupdate'),
    path('equipment/pricetag/update_form/<int:id>', views.eslupdateform, name='eslupdateform'),
    path('equipment/esltable', views.esl_table, name='esl_table'),
    path('equipment/pricetag/esldelete/<int:id>', views.esldelete, name='esl_delete'),
path('equipment/pricetag/view_form/<int:id>', views.eslviewform, name='eslviewform'),

    path('store', views.store, name='store'),
    path('store/update/<int:id>', views.storeupdate, name='storeupdate'),
    path('store/storeupdateform/<int:id>', views.storeupdateform, name='storeupdateform'),
    path('store/storedelete/<int:id>', views.storedelete, name='store_delete'),

    

    
    # url(r'^password_reset/$', auth_views.PasswordResetView.as_view(), name='password_reset'),
    # url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    # url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    #     auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    # url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    path("password_reset", views.password_reset_request, name="password_reset"),
    path('password_reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='main/password/password_reset_done.html'),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name="main/password/password_reset_confirm.html"),
         name='password_reset_confirm'),
    path('reset/done/',
         auth_views.PasswordResetCompleteView.as_view(template_name='main/password/password_reset_complete.html'),
         name='password_reset_complete'),
    path('store/storeviewform/<int:id>', views.storeviewform, name='storeviewform'),

]
