from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect, get_object_or_404
import requests
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from base_station.models import BaseStation
from base_station.forms import UpdateBaseStationForm, Updateesl, Commodity_form, UpdateTemplate
from base_station.models import BaseStation, Store
from base_station.forms import UpdateBaseStationForm, Updateesl, Commodity_form, UpdateTemplate, Viewesl, Viewbasestation
from commodity_api.models import Commodities
from commodity_api.forms import UpdateCommoditiesForm, ViewCommoditiesForm
# Create your views here.
from esl_api.models import ESL
from template_api.models import Template
from .forms import UserRegForm, ChangePwdForm, UserForm
from esl_api.forms import StoreForm, eslForm
from .forms import UserRegForm
from esl_api.forms import StoreForm, eslForm, ViewStoreForm
from base_station.forms import AddBaseStationForm
from .models import Esl_User

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.contrib import auth, messages
from django.contrib.auth.hashers import check_password

from django.contrib.auth.decorators import login_required

from django.shortcuts import render, redirect
from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.http import HttpResponse
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.db.models.query_utils import Q
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes

from .token_generator import account_activation_token


@login_required(login_url = "/signin")
def home(request):
    store = Store.objects.all()
    return render(request,'home/index.html', {"stores": store})

def password_reset_request(request):
    if request.method == "POST":
        password_reset_form = PasswordResetForm(request.POST)
        if password_reset_form.is_valid():
            print("password_reset_form.cleaned_data['email']",password_reset_form.cleaned_data['email'] )
            data = password_reset_form.cleaned_data['email']
            
            associated_users = User.objects.filter(Q(email=data))
            print(associated_users.exists())
            if associated_users.exists():
                for user in associated_users:
                    subject = "Password Reset Requested"
                    email_template_name = "main/password/password_reset_email.txt"
                    c = {
                    "email":user.email,
                    'domain':'127.0.0.1:8000',
                    'site_name': 'Website',
                    "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                    "user": user,
                    'token': default_token_generator.make_token(user),
                    'protocol': 'http',
                    }
                    email = render_to_string(email_template_name, c)
                    print(email)
                    try:
                        send_mail(subject, email, 'admin@example.com' , [user.email], fail_silently=False)
                    except BadHeaderError:
                        return HttpResponse('Invalid header found.')
                    return redirect ("/password_reset/done/")
    password_reset_form = PasswordResetForm()
    return render(request=request, template_name="main/password/password_reset.html", context={"password_reset_form":password_reset_form})
 
@login_required(login_url = "/signin")
def basestation(request):
    updateform  = UpdateBaseStationForm(initial={"Inked_esl_amount" : "-1",
                                                "code":"-1"})
    adddform = AddBaseStationForm()
    if request.method == 'POST':
        r = requests.post('http://127.0.0.1:8000/base_station/add', data=request.POST)
        if r.status_code == 200:
            pass        
        queryset = BaseStation.objects.all()
        return redirect('basestation')
    else:
        # updateform = UpdateBaseStationForm()
        # adddform = AddBaseStationForm()
        queryset = BaseStation.objects.all()        
        return render(request,'home/basestation.html',{'base_station': queryset, 
                                                        'updateform' : updateform,
                                                        'adddform': adddform})
 

def basestationviewform(request, id):
   entry = get_object_or_404(BaseStation, id=id)
   if request.method != 'POST':
       form = Viewbasestation(instance=entry)
       return HttpResponse(form.as_p())   # This will return the plain html of a form


def basestationupdateform(request, id):
   
   entry = get_object_or_404(BaseStation, id=id)

   if request.method != 'POST':
       form = UpdateBaseStationForm(instance=entry)
       return HttpResponse(form.as_p())   # This will return the plain html of a form
   

@login_required(login_url = "/signin")
def basestationupdate(request,id):
    if request.method == 'POST':        
        r = requests.post(f'http://127.0.0.1:8000/base_station/updatedata/{id}', data=request.POST)
        if r.status_code == 200:
            pass        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/basestation'))
        
@login_required(login_url = "/signin")
def basestationdelete(request,id):
    if request.method == 'GET':        
        r = requests.get(f'http://127.0.0.1:8000/base_station/delete/{id}')
        if r.status_code == 200:
            pass        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/basestation'))

@login_required(login_url = "/signin")
def esl(request):
    esls = ESL.objects.all()
    add_esl_form = eslForm()
    special_fees_form = Updateesl()
        # initial={"esl_size" : "-1",
        #                                     "powermeter" : "-1"})

    if request.method == 'POST':        
        r = requests.post('http://127.0.0.1:8000/esl_api/add', data=request.POST)
        if r.status_code == 200:
            pass                
        return redirect('esl')
    else:
        return render(request,'home/esl.html',{'esls':esls,'form': special_fees_form, 'add_form': add_esl_form})


def eslviewform(request, id):
   entry = get_object_or_404(ESL, id=id)
   if request.method != 'POST':
       form = Viewesl(instance=entry)
       return HttpResponse(form.as_p())   # This will return the plain html of a form


def eslupdateform(request, id):
   
   entry = get_object_or_404(ESL, id=id)

   if request.method != 'POST':
       form = Updateesl(instance=entry)
       return HttpResponse(form.as_p())   # This will return the plain html of a form
   

@login_required(login_url = "/signin")
def eslupdate(request,id):
    if request.method == 'POST':        
        r = requests.post(f'http://127.0.0.1:8000/esl_api/updatedata/{id}', data=request.POST)
        if r.status_code == 200:
            pass        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/equipment/pricetag'))

@login_required(login_url = "/signin")
def esldelete(request,id):
    if request.method == 'GET':        
        r = requests.get(f'http://127.0.0.1:8000/esl_api/delete/{id}')
        if r.status_code == 200:
            pass        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/equipment/pricetag'))


@login_required(login_url = "/signin")
def store(request):
    stores = Store.objects.all()
    form = StoreForm()
    if request.method == 'POST':        
        r = requests.post('http://127.0.0.1:8000/esl_api/store_add', data=request.POST)    
        if r.status_code == 200:
            pass                
        return redirect('store')
    else:
        return render(request,'home/store.html',{'stores':stores,'form': form})


def storeviewform(request, id):
   entry = get_object_or_404(Store, id=id)
   if request.method != 'POST':
       form = ViewStoreForm(instance=entry)
       return HttpResponse(form.as_p())   # This will return the plain html of a form


def storeupdateform(request, id):
   
   entry = get_object_or_404(Store, id=id)

   if request.method != 'POST':
       form = StoreForm(instance=entry)
       return HttpResponse(form.as_p())   # This will return the plain html of a form


@login_required(login_url = "/signin")
def storeupdate(request,id):
    if request.method == 'POST':        
        r = requests.post(f'http://127.0.0.1:8000/esl_api/store_updatedata/{id}', data=request.POST)
        if r.status_code == 200:
            pass        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/store'))

@login_required(login_url = "/signin")
def storedelete(request,id):
    if request.method == 'GET':        
        r = requests.get(f'http://127.0.0.1:8000/esl_api/store_delete/{id}')
        if r.status_code == 200:
            pass        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/store'))



@login_required(login_url = "/signin")
def commodities(request):
    updateform  = UpdateCommoditiesForm(initial={"selling_price" : "-1",
                                                "original_price" : "-1",
                                                "vip_price" : "-1",
                                                "stock_1" : "-1",
                                                "stock_2" : "-1",
                                                "stock_3" : "-1",})
    queryset = Commodities.objects.all() 
    if request.method == 'POST':        
        r = requests.post('http://127.0.0.1:8000/commodity_api/add', data=request.POST, files =request.FILES)
        if r.status_code == 200:
            pass        
        queryset = Commodities.objects.all()                
        return render(request,'home/commodities.html',{'commodity': queryset,
                                                        'updateform' : updateform})
    else:
        queryset = Commodities.objects.all()        
        return render(request,'home/commodities.html',{'commodity': queryset,
                                                        'updateform' : updateform})
                                                        


def commodityviewform(request, id):
   entry = get_object_or_404(Commodities, id=id)
   if request.method != 'POST':
       form = ViewCommoditiesForm(instance=entry)
       return HttpResponse(form.as_p())   # This will return the plain html of a form


def commoditieupdateform(request, id):
   
   entry = get_object_or_404(Commodities, id=id)

   if request.method != 'POST':
       form = UpdateCommoditiesForm(instance=entry)
       return HttpResponse(form.as_p())   # This will return the plain html of a form
    


@login_required(login_url = "/signin")
def commoditiesupdate(request,id):
    if request.method == 'POST':        
        r = requests.post(f'http://127.0.0.1:8000/commodity_api/updatedata/{id}', data=request.POST, files =request.FILES)
        if r.status_code == 200:
            pass        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', 'productmanager/productquery'))

@login_required(login_url = "/signin")
def commoditiesdelete(request,id):
    if request.method == 'GET':        
        r = requests.get(f'http://127.0.0.1:8000/commodity_api/delete/{id}')
        if r.status_code == 200:
            pass        
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', 'productmanager/productquery'))

@login_required(login_url = "/signin")
def storetempate(request):
    template = Template.objects.all()
    form = UpdateTemplate()

    if request.method == 'POST':        
        r = requests.post('http://127.0.0.1:8000/template_api/add', data=request.POST)
        if r.status_code == 200:
            pass                
        return redirect('storetempate')
    else:
        return render(request,'home/storetemplate.html',{'templates':template,'form':form})
 
@login_required(login_url = "/signin")
def edittemplate(request):
    return render(request,'home/edittemplate.html')



# MEEEEEEEEEEEEEEEEEEEEEEEEEEEee

# def eslupdate(request,id):
#     esl = get_object_or_404(ESL, id=id)

#     special_fees_form = Updateesl(initial=esl.__dict__)
#     if request.method == 'POST':

#         special_fees_form = Updateesl(request.POST)

#         if special_fees_form.is_valid():
#             # process form data
#             obj = esl.objects.get(id=id)  # gets new object
#             obj.commodity_barcode = special_fees_form.cleaned_data['commodity_barcode']
#             obj.esl_barcode = special_fees_form.cleaned_data['esl_barcode']
#             obj.store = special_fees_form.cleaned_data['store']

#             obj.save()

#             return redirect('esl')

#     print(special_fees_form)
    # return render(request, 'home/esl.html', {'form': special_fees_form})

#
# #MEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEeeeee
@login_required(login_url = "/signin")
def esl_table(request):

    return render(request,'home/esltable.html',
                  # {'form':form}
                  )


@login_required(login_url = "/signin")
def editcommodity(request,id):

    commodity = get_object_or_404(Commodities, id=id)
    form = Commodity_form(instance=commodity)
    if form.is_valid():
        # process form data
        form.save()
        print("saved")
        return redirect('commodities')
    else:
        print("errors",form.errors)

    return render(request, 'home/updateproduct.html', {'form': form})

@login_required(login_url = "/signin")
def create_template(request):
    return render(request,'home/createtemplate.html')


def sign_up(request):
    if request.method == 'POST':

        form = UserRegForm(request.POST)

        if form.is_valid():
            if request.POST.get('password') != request.POST.get('confirm_password'):
                messages.error(request, "Password does not match")                
                return redirect('signup')
            elif User.objects.filter(username=request.POST.get('username')).exists():
                messages.error(request, "This User Name already exists.")
                return redirect('signup')
            else:

                form = UserRegForm()

                userdata = User()
                userdata.username = request.POST.get('username')
                userdata.password = make_password(request.POST.get('password'))
                userdata.email = request.POST.get('username')
                userdata.is_active = False

                userdata.save()

                esldata = Esl_User()            
                esldata.firstName = request.POST.get('firstName')
                esldata.lastName = request.POST.get('lastName')
                esldata.mobileNo = request.POST.get('mobileNo')
                esldata.user_id = userdata.id
                esldata.save()
                # return redirect('signin')


                # Sending activation part
                current_site = get_current_site(request)
                email_subject = 'Activate Your Account'
                message = render_to_string('registration/activate_account.html', {
                    'user': userdata,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(userdata.pk)),
                    'token': account_activation_token.make_token(userdata),
                })
                to_email = request.POST.get('username')
                email = EmailMessage(email_subject, message, to=[to_email])

                email.send()
                messages.success(request,
                    'We have sent you an email, please confirm your email address to complete registration')
                return redirect('signup')




        else:
            messages.error(request, "Make sure your password length is alteast 8 char")
    form = UserRegForm()
    return render(request, 'home/register.html', {'form': form})


def activate_account(request, uidb64, token):
    try:
        uid = force_bytes(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        # sign_in(request)
        messages.success(request,'Your account has been activate successfully')
        return redirect('signin')
    else:
        messages.success(request,'Activation link is invalid!')

        return redirect('signup')

def sign_in(request):
    if request.method == 'POST':
        userdata = auth.authenticate(username=request.POST.get('user_name'), password=request.POST.get('password'))
        if userdata is not None:
            auth.login(request, userdata)
            username1 = str(request.POST.get('user_name'))
            # print(userdata.groups.all[0])
            return redirect('index')
        else:
            messages.error(request, "Incorrect username or password")
            return redirect('signin')
    return render(request,'home/sign_in.html')

def logoutfun(request):
    auth.logout(request)
    return redirect('signin')

def profile_update(request):
    print("USER: ", request.user.id)

    if request.method == 'POST':

        form = ChangePwdForm(request.POST)

        if form.is_valid():
            new_password = request.POST.get('new_password', '')
            User.objects.filter(id=request.user.id).update(password=make_password(new_password))

            # company.objects.filter(id = request.user.company_data.id).update(is_change_pwd = True)

            messages.success(request, 'Password successfully changed.')
            auth.logout(request)
            return redirect(reverse('signin'))

    else:
        form = ChangePwdForm()


    return render(request,'home/profile update.html',{'form':form})



def user_details_update(request, id):
    print("USER: ", request.user.id)

    fee = get_object_or_404(Esl_User, id=id)

    initial = {
        'firstName': fee.firstName,
        'lastName': fee.lastName,
        'mobileNo': fee.mobileNo
    }

    form = UserForm(initial=initial)

    if request.method == 'POST':

        form = UserForm(request.POST)

        if form.is_valid():
            # process form data
            obj = Esl_User.objects.get(id=id)  # gets new object
            obj.firstName = form.cleaned_data['firstName']
            obj.lastName = form.cleaned_data['lastName']
            obj.mobileNo = form.cleaned_data['mobileNo']

            obj.save()

            messages.success(request, 'User details successfully!')
            return redirect('index')

    return render(request, 'home/updateuser.html', {'form': form, 'student': fee})
