from django.db import models
from django.contrib.auth.models import User

class Esl_User(models.Model):
  user = models.OneToOneField(User, related_name="Esl_user", on_delete=models.CASCADE)  
  firstName = models.CharField(max_length=100, verbose_name= "First Name")
  lastName = models.CharField(max_length=100, verbose_name= "Last Name")
  mobileNo = models.CharField(max_length=13,verbose_name='Mobile Number*')
  role = models.BooleanField(default=False)
  isDeleted = models.BooleanField(default=False)