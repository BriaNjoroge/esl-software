from rest_framework import serializers
from . import models

class BaseSationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BaseStation
        fields = "__all__"