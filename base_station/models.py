from django.db import models

# Create your models here.


class Store(models.Model):
    facility_address = models.CharField('facility address', max_length=255, null=True, blank=True)
    facility_name = models.CharField('facility name', max_length=255, null=True, blank=True)

    def __str__(self):
        return self.facility_name


class BaseStation(models.Model):
    store = models.ForeignKey(Store,related_name="store" ,on_delete=models.CASCADE)
    code = models.IntegerField('code',default=123)
    Ap_name = models.CharField('Ap Name', max_length=255, null=True, blank=True)
    Ap_mac = models.CharField('Ap Mac', max_length=255, null=True, blank=True)
    model_firmware_version = models.CharField('Model Firmware Version', max_length=255, null=True, blank=True)
    Inked_esl_amount = models.FloatField('Inked esl amount',default=0)
    Status = models.BooleanField('Status', default=False, null=True)
    Online_time	= models.DateTimeField('Online Time', null=True, blank=True)
    Offline_time = models.DateTimeField('Ofline Time', null=True, blank=True)
    wifi_2g_name = models.CharField('Wifi 2g name', max_length=255, null=True, blank=True)
    wifi_5g_name = models.CharField('Wifi 5g name', max_length=255, null=True, blank=True)
    wifi_set_status	= models.BooleanField('Wifi Set Status', default=False, null=True)
    wifi_set_time = models.DateTimeField("Wifi Set Time", null=True, blank=True)
    Comment = models.CharField('Comment', max_length=255, null=True, blank=True)
    asset_location =  models.CharField('Assest Location', max_length=255, null=True, blank=True)
 
    
    def __str__(self):
        return str(self.code)