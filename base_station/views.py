from django.shortcuts import render
from base_station.models import BaseStation
from base_station.forms import AddBaseStationForm
# Create your views here.

import json
import time
import requests
from django.core import serializers
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView

from django.contrib import messages
from django.conf import settings
from django.db import IntegrityError
from django.http import HttpResponse, FileResponse, Http404, JsonResponse
import os
from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.decorators import api_view

from django_filters.rest_framework import DjangoFilterBackend
from django.views.decorators.csrf import csrf_exempt
from .serializers import BaseSationSerializer

@csrf_exempt
@api_view(['GET'])
def base_station_get(request):
    queryset = BaseStation.objects.all()    
    serializer = BaseSationSerializer(queryset, many=True)
    return JsonResponse({'base_station': serializer.data}, safe=False, status=status.HTTP_200_OK)    

@api_view(['GET'])
def base_station_delete(request, id):
    base_station = get_object_or_404(BaseStation, id=id)
    BaseStation.objects.filter(id=id).delete()
    context = {
        'status': 'success',
        'Message': f"Job description {base_station.id} has been deleted"
    }
    return Response(context)


@csrf_exempt
@api_view(['POST'])
def base_station_update(request,id):
    if request.method == 'POST':
        print(request.POST.get('Status'))
        base_station = get_object_or_404(BaseStation, id=id)        
        base_station.code  = request.POST.get('code') if request.POST.get('code') != "-1" else base_station.code
        base_station.store_id  = request.POST.get('store') if request.POST.get('store') != "" else base_station.store
        base_station.Ap_name  = request.POST.get('Ap_name') if request.POST.get('Ap_name') != "" else base_station.Ap_name
        base_station.Ap_mac  = request.POST.get('Ap_mac') if request.POST.get('Ap_mac') != "" else base_station.Ap_mac
        base_station.model_firmware_version  = request.POST.get('model_firmware_version') if request.POST.get('model_firmware_version') != "" else base_station.model_firmware_version
        base_station.Inked_esl_amount  = request.POST.get('Inked_esl_amount') if request.POST.get('Inked_esl_amount') != "-1" else base_station.Inked_esl_amount
        if request.POST.get('Status') != "":
            if request.POST.get('Status') == 'true':
                base_station.Status = 1
            else:
                base_station.Status = 0
        base_station.Online_time  = request.POST.get('Online_time') if request.POST.get('Online_time') != "" else base_station.Online_time
        base_station.Offline_time  = request.POST.get('Offline_time') if request.POST.get('Offline_time') != "" else base_station.Offline_time
        base_station.wifi_2g_name  = request.POST.get('wifi_2g_name') if request.POST.get('wifi_2g_name') != "" else base_station.wifi_2g_name
        base_station.wifi_5g_name  = request.POST.get('wifi_5g_name') if request.POST.get('wifi_5g_name') != "" else base_station.wifi_5g_name
        # base_station.wifi_set_status  = request.POST.get('wifi_set_status') if request.POST.get('wifi_set_status') != "" else base_station.wifi_set_status
        if request.POST.get('wifi_set_status') != "":
            if request.POST.get('wifi_set_status') == 'true':
                base_station.wifi_set_status = 1
            else:
                base_station.wifi_set_status = 0
        base_station.wifi_set_time  = request.POST.get('wifi_set_time') if request.POST.get('wifi_set_time') != "" else base_station.wifi_set_time
        base_station.Comment  = request.POST.get('Comment') if request.POST.get('Comment') != "" else base_station.Comment
        base_station.asset_location  = request.POST.get('asset_location') if request.POST.get('asset_location') != "" else base_station.asset_location
        
        base_station.save()
        context = {
            'id': base_station.id,                
        }
        return Response(context)

@csrf_exempt
@api_view(['POST'])
def base_add(request):
    if request.method == 'POST':
        print(request.POST)
        form = AddBaseStationForm(request.POST)
        if form.is_valid():
            base_station = BaseStation()            
            base_station.store_id  = request.POST.get('store')
            base_station.Ap_name  = request.POST.get('Ap_name')
            base_station.Ap_mac  = request.POST.get('Ap_mac')            
            base_station.Comment  = request.POST.get('Comment')
            base_station.save()
            context = {
                'id': base_station.id,                
            }
            return Response(context)
        else:        
            return Response({"status": "fail",
                            "message": "form is invalid"})
    
    else:
        
        return Response({"status": "fail",
                         "message": "method is not allowed"})
    