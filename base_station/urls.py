
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('add', views.base_add, name='addbasestation'),    
    path('getdata', views.base_station_get, name='getbasestation'),    
    path('updatedata/<int:id>', views.base_station_update, name='updatebase_station'),    
    path('delete/<int:id>', views.base_station_delete, name='deletebasestation'),    
]
 