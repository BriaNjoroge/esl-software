from django import forms
from django.forms import ClearableFileInput
from base_station.models import BaseStation
from commodity_api.models import Commodities
from esl_api.models import ESL
from template_api.models import Template

class DateInput(forms.DateInput):
    input_type = 'date'
    # input_formats = ['%Y-%m-%d %H:%M']

class AddBaseStationForm(forms.ModelForm):
    class Meta:
        model = BaseStation
        fields = ['store','Ap_name','Ap_mac','Comment']
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class Viewbasestation(forms.ModelForm):
    class Meta:
        model = BaseStation
        fields = '__all__'
        widgets = {
            'Online_time': DateInput(),
            'Offline_time': DateInput(),
            'wifi_set_time': DateInput(),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            self.fields[field].disabled = True        


class UpdateBaseStationForm(forms.ModelForm):
    class Meta:
        model = BaseStation
        fields = '__all__'
        widgets = {
            'Online_time': DateInput(),
            'Offline_time': DateInput(),
            'wifi_set_time': DateInput(),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
    


class Viewesl(forms.ModelForm):
    class Meta:
        model = ESL
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        choices = ((1.54,1.54),
         (2.19 ,2.19) ,
         (2.79 ,2.79) )
        self.fields['esl_size'] = forms.ChoiceField(choices = choices) 
        self.fields['powermeter'] = forms.ChoiceField(choices = choices) 
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            self.fields[field].disabled = True        

class Updateesl(forms.ModelForm):
    class Meta:
        model = ESL
        fields = '__all__'

    def clean(self):
        cleaned_data = super(Updateesl, self).clean()
        commodity_barcode = cleaned_data.get("commodity_barcode")
        esl_barcode = cleaned_data.get("esl_barcode")
        store = cleaned_data.get("store")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        choices = ((1.54,1.54),
         (2.19 ,2.19) ,
         (2.79 ,2.79) )
        self.fields['esl_size'] = forms.ChoiceField(choices = choices) 
        self.fields['powermeter'] = forms.ChoiceField(choices = choices) 
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        

class Commodity_form(forms.ModelForm):
    class Meta:
        model = Commodities
        exclude = ['picture_info']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class UpdateTemplate(forms.ModelForm):
    class Meta:
        model = Template
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})