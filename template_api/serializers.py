from rest_framework import serializers
from . import models

class TemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Template
        fields = "__all__"