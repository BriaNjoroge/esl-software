from django.apps import AppConfig


class TemplateApiConfig(AppConfig):
    name = 'template_api'
