from django.shortcuts import render
from template_api.models import Template
# Create your views here.

import json
import time
import requests
from django.core import serializers
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView

from django.contrib import messages
from django.conf import settings
from django.db import IntegrityError
from django.http import HttpResponse, FileResponse, Http404, JsonResponse
import os
from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework import status, generics
from rest_framework.decorators import api_view

from django_filters.rest_framework import DjangoFilterBackend
from django.views.decorators.csrf import csrf_exempt
from .serializers import TemplateSerializer

@csrf_exempt
@api_view(['GET'])
def template_get(request):
    queryset = Template.objects.all()    
    serializer = TemplateSerializer(queryset, many=True)
    return JsonResponse({'template': serializer.data}, safe=False, status=status.HTTP_200_OK)    

@api_view(['GET'])
def template_delete(request, id):
    template = get_object_or_404(Template, id=id)
    Template.objects.filter(id=id).delete()
    context = {
        'status': 'success',
        'Message': f"Job description {template.id} has been deleted"
    }
    return Response(context)


@csrf_exempt
@api_view(['POST'])
def template_update(request,id):
    if request.method == 'POST':
        print(request.POST.get('Status'))
        template = get_object_or_404(Template, id=id)        
        template.code  = request.POST.get('code') if request.POST.get('code') != "" else template.code
        template.Ap_name  = request.POST.get('Ap_name') if request.POST.get('Ap_name') != "" else template.Ap_name
        template.Ap_mac  = request.POST.get('Ap_mac') if request.POST.get('Ap_mac') != "" else template.Ap_mac
        template.model_firmware_version  = request.POST.get('model_firmware_version') if request.POST.get('model_firmware_version') != "" else template.model_firmware_version
        template.Inked_esl_amount  = request.POST.get('Inked_esl_amount') if request.POST.get('Inked_esl_amount') != "" else template.Inked_esl_amount
        if request.POST.get('Status') != "":
            if request.POST.get('Status') == 'true':
                template.Status = 1
            else:
                template.Status = 0
        template.Online_time  = request.POST.get('Online_time') if request.POST.get('Online_time') != "" else template.Online_time
        template.Offline_time  = request.POST.get('Offline_time') if request.POST.get('Offline_time') != "" else template.Offline_time
        template.wifi_2g_name  = request.POST.get('wifi_2g_name') if request.POST.get('wifi_2g_name') != "" else template.wifi_2g_name
        template.wifi_5g_name  = request.POST.get('wifi_5g_name') if request.POST.get('wifi_5g_name') != "" else template.wifi_5g_name
        # template.wifi_set_status  = request.POST.get('wifi_set_status') if request.POST.get('wifi_set_status') != "" else template.wifi_set_status
        if request.POST.get('wifi_set_status') != "":
            if request.POST.get('wifi_set_status') == 'true':
                template.wifi_set_status = 1
            else:
                template.wifi_set_status = 0
        template.wifi_set_time  = request.POST.get('wifi_set_time') if request.POST.get('wifi_set_time') != "" else template.wifi_set_time
        template.Comment  = request.POST.get('Comment') if request.POST.get('Comment') != "" else template.Comment
        template.save()
        context = {
            'id': template.id,                
        }
        return Response(context)

@csrf_exempt
@api_view(['POST'])
def template_add(request):
    if request.method == 'POST':
        print(request.POST)
        # form = AddBaseStationForm(request.POST)
        # if form.is_valid():
        template = Template()  
        template.template_name = request.POST.get('template_name')
        template.dpi = request.POST.get('dpi')
        template.size = request.POST.get('size')
        template.color = request.POST.get('color')
        template.model = request.POST.get('model')
        template.template_category = request.POST.get('template_category')
        template.template_type = request.POST.get('template_type')
        
        template.save()
        context = {
            'id': template.id,                
        }
        return Response(context)
        # else:        
        #     return Response({"status": "fail",
        #                     "message": "form is invalid"})
    
    else:
        
        return Response({"status": "fail",
                         "message": "method is not allowed"})
    