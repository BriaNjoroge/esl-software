from django.db import models

# Create your models here.

class Template(models.Model):
    template_name = models.CharField('Template name', max_length=255, null=True, blank=True)
    dpi = models.IntegerField('Dpi',default=123)
    size = models.CharField('Size', max_length=255, null=True, blank=True)
    color = models.CharField('Color', max_length=255, null=True, blank=True)
    model = models.CharField('Model', max_length=255, null=True, blank=True)
    template_category = models.CharField('Template category', max_length=255, null=True, blank=True)
    template_type = models.CharField('Template type', max_length=255, null=True, blank=True)
    