
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('add', views.template_add, name='addtemplate'),    
    path('getdata', views.template_get, name='gettemplate'),    
    path('updatedata/<int:id>', views.template_update, name='updatetemplate'),    
    path('delete/<int:id>', views.template_delete, name='deletetemplate'),    
]
 